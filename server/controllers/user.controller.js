import {ApiError} from "../error/api.error.js";
import bcrypt from 'bcrypt';
import {Basket, User} from "../models/relations.js";
import {generateJwt} from "../helpers/generateJwt.js";

class UserController {

    async registration(req, res, next) {
        const {email, password, role} = req.body;
        if (!email || !password) {
            return next(ApiError.badRequest('Incorrect email or password'));
        }
        const candidate = await User.findOne({where: {email}});
        if (candidate) {
            return next(ApiError.badRequest('User with this email already exist'));
        }
        const hashPassword = await bcrypt.hash(password, 5);
        const user = await User.create({email, role, password: hashPassword});
        const basket = await Basket.create({userId: user.id});
        return res.json({message: 'User has been registered'});
    }

    async login(req, res, next) {
        const {email, password} = req.body;
        const user = await User.findOne({where: {email}});
        if (!user) {
            return next(ApiError.internal('User not found'));
        }
        let comparePassword = bcrypt.compareSync(password, user.password);
        if (!comparePassword) {
            return next(ApiError.internal('Incorrect password'));
        }
        const token = generateJwt(user.id, user.email, user.role);
        return res.json({token});
    }

    async checkUser(req, res, next) {
        const {id, email, role} = req.user;
        const token = generateJwt(id, email, role);
        res.json({token});
    }

}

export const userController = new UserController();
