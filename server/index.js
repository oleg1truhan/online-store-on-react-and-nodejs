import 'dotenv/config';
import express from 'express';
import {sequelize} from './sequelize.js';
import './models/relations.js';
import cors from 'cors';
import fileupload from 'express-fileupload';
import {router} from './routers/index.js';
import {errorHandler} from "./middleware/ErrorHandlingMiddleware.js";
import path from 'path';
import {fileURLToPath} from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const PORT = process.env.PORT || 5000;

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static(path.resolve(__dirname, 'static')));
app.use(fileupload({}));
app.use('/api', router);

app.use(errorHandler);

(async () => {
    try {
        await sequelize.authenticate();
        await sequelize.sync({force: true});
        app.listen(PORT, () => console.log(`Server has been started on ${PORT}`));
    } catch (e) {
        console.log(e);
    }
})();
