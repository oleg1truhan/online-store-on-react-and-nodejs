import {sequelize} from '../sequelize.js';
import {DataTypes} from "sequelize";

export const BasketDevice = sequelize.define('basket_device', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
}, {
    timestamps: false
});
