import {sequelize} from '../sequelize.js';
import {DataTypes} from "sequelize";

export const Basket = sequelize.define('basket', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
}, {
    timestamps: false
});
