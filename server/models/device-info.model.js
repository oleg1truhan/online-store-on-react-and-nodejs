import {sequelize} from '../sequelize.js';
import {DataTypes} from "sequelize";

export const DeviceInfo = sequelize.define('deviceInfo', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    title: {type: DataTypes.STRING, allowNull: false},
    description: {type: DataTypes.STRING, allowNull: false}
}, {
    timestamps: false
});
