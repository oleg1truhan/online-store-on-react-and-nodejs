import {sequelize} from '../sequelize.js';
import {DataTypes} from "sequelize";

export const Rating = sequelize.define('rating', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    rate: {type: DataTypes.INTEGER, allowNull: false}
}, {
    timestamps: false
});
