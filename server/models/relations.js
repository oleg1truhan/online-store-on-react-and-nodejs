import {User} from "./user.model.js";
import {Basket} from "./basket.model.js";
import {Device} from "./device.model.js";
import {DeviceInfo} from "./device-info.model.js";
import {BasketDevice} from "./basket-device.model.js";
import {Rating} from "./rating.model.js";
import {Type} from "./type.model.js";
import {Brand} from "./brand.model.js";
import {TypeBrand} from "./type-brand.model.js";

User.hasOne(Basket);
Basket.belongsTo(User);

User.hasMany(Rating);
Rating.belongsTo(User);

Basket.hasMany(BasketDevice);
BasketDevice.belongsTo(Basket);

Type.hasMany(Device);
Device.belongsTo(Type);

Brand.hasMany(Device);
Device.belongsTo(Brand);

Device.hasMany(Rating);
Rating.belongsTo(Device);

Device.hasMany(BasketDevice);
BasketDevice.belongsTo(Device);

Device.hasMany(DeviceInfo, {as: 'info'});
DeviceInfo.belongsTo(Device);

Type.belongsToMany(Brand, {through: TypeBrand});
Brand.belongsToMany(Type, {through: TypeBrand});

export {
    User,
    Basket,
    BasketDevice,
    Device,
    Type,
    Brand,
    Rating,
    TypeBrand,
    DeviceInfo
};
