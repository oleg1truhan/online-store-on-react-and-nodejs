import {Router} from 'express';
import {brandController} from "../controllers/brand.controller.js";
import {checkRoleMiddleware} from '../middleware/checkRoleMiddleware.js';

export const brandRouter = new Router();

brandRouter.post('/', checkRoleMiddleware('ADMIN'), brandController.create);
brandRouter.get('/', brandController.getAll);
