import {Router} from 'express';
import {userRouter} from "./user.router.js";
import {brandRouter} from "./brand.router.js";
import {typeRouter} from "./type.router.js";
import {deviceRouter} from "./device.router.js";

export const router = new Router();

router.use('/user', userRouter);
router.use('/type', typeRouter);
router.use('/brand', brandRouter);
router.use('/device', deviceRouter);
